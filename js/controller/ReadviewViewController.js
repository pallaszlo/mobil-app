/**
 * @author Jörn Kreutel
 * Edited by Nils Kretschmer / Laszlo Pal
 */
define(["mwf", "entities"], function (mwf, entities) {

    class ReadviewViewController extends mwf.ViewController {

        constructor() {
            super();
            this.viewProxy = null;
        }
        /*
         * for any view: initialise the view
         */
        oncreate(callback) {
            // TODO: do databinding, set listeners, initialise the view
            var mediaItem = this.args.item;

            //template element initialise
            this.viewProxy = this.bindElement("mediaReadviewTemplate",this.args,this.root).viewProxy;

            this.addListener(new mwf.EventMatcher("crud","updated","MediaItem"),((event) => {
                this.viewProxy.update({item: mediaItem});
            }));
            //click on selected element will open the next view
            this.viewProxy.bindAction("editMediaItem",() => {
                this.nextView("mediaEditview",this.args)
            });
            //binding function for delete the selected item
            this.viewProxy.bindAction("deleteMedia",() => {
                this.args.item.delete(() => {
                    this.previousView();
                });
            });

            // call the superclass once creation is done
            super.oncreate(callback);
        }

        //stop the playing mediaItem when changed the view
        onpause (callback) {
            var video = this.root.querySelector("video");
            if (video) {
                console.log("calling pause() on video: " + video);
                video.pause();
            }
            super.onpause(callback);
        }

        /*
         * for views with listviews: bind a list item to an item view
         * TODO: delete if no listview is used or if databinding uses ractive templates
         */
        bindListItemView(viewid, itemview, item) {
            // TODO: implement how attributes of item shall be displayed in itemview
        }

        /*
         * for views with listviews: react to the selection of a listitem
         * TODO: delete if no listview is used or if item selection is specified by targetview/targetaction
         */
        onListItemSelected(listitem, listview) {
            // TODO: implement how selection of listitem shall be handled

        }

        /*
         * for views with listviews: react to the selection of a listitem menu option
         * TODO: delete if no listview is used or if item selection is specified by targetview/targetaction
         */
        onListItemMenuItemSelected(option, listitem, listview) {
            // TODO: implement how selection of option for listitem shall be handled
        }

        /*
         * for views with dialogs
         * TODO: delete if no dialogs are used or if generic controller for dialogs is employed
         */
        bindDialog(dialogid, dialog, item) {
            // call the supertype function
            super.bindDialog(dialogid, dialog, item);

            // TODO: implement action bindings for dialog, accessing dialog.root
        }
    }
    // and return the view controller function
    return ReadviewViewController;
});