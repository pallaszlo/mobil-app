
/**
 * @author Jörn Kreutel
 * Edited by Nils Kretschmer / Laszlo Pal
 */
define(["mwf", "entities"], function (mwf, entities) {

    class EditviewViewController extends mwf.ViewController {

        constructor() {
            super();
            console.log("EditviewViewController()");
        }
        /*
         * for any view: initialise the view
         */

        oncreate(callback) {

            // TODO: do databinding, set listeners, initialise the view
            var mediaItem = this.args.item;
            this.viewProxy = this.bindElement("mediaEditviewTemplate", this.args, this.root).viewProxy;

            //function for updating the database
            this.addListener(new mwf.EventMatcher("crud","updated","MediaItem"),((event) => {
                this.viewProxy.update({item: mediaItem});
            }));

            this.uploadElement = this.root.querySelector("#upload");
            this.uploadLabel = this.root.querySelector("#uploadLabel");
            // check for local/remote and hide/show upload elements
            if (this.application.currentCRUDScope == "local") {
                this.uploadLabel.style.visibility = "hidden";
                this.uploadElement.style.visibility = "hidden";
            } else {
                this.uploadElement.style.visibility = "visible";
                this.uploadLabel.style.visibility = "visible";
            }

            // delete function by binding
            this.viewProxy.bindAction("deleteItem", () => {
                mediaItem.delete(() => {
                    // return to the list view
                    this.nextView("mediaOverview",{item: new entities.MediaItem()});
                });

            });
            //function for create, update and upload of Mediaitems to the database
            this.viewProxy.bindAction("submitMediaForm", (event) => {
                event.original.preventDefault();
                var editForm = this.root.ownerDocument.getElementById("newMediaItem");
                console.log("MediaItem srctype:" + mediaItem.srctype);
                if(mediaItem.srctype == "upload"){
                    //request-object initialised
                    var xhr = new XMLHttpRequest();
                    //callback function for response
                    xhr.onreadystatechange = function(){
                        // response available
                        if(xhr.readyState == 4){
                            // request data ok
                            if(xhr.status ==200){
                                // Parse the response and set attributes on mediaItem
                                var receivedData = JSON.parse(xhr.responseText);
                                mediaItem.src = receivedData.data.srccontent;
                                mediaItem.contentType = receivedData.data.contentType;
                                // create and go back to previus view
                                if (!mediaItem.created) {
                                    mediaItem.create(() => {
                                        this.viewProxy.update({item: mediaItem});
                                        this.previousView();
                                    });
                                }
                                else {
                                    // update the mediaItem
                                    mediaItem.update(() => {
                                        this.viewProxy.update({item: mediaItem});
                                        this.previousView();
                                    });
                                }
                            }
                            else {
                                alert("error response from server: " + xhr.status);
                            }
                        }
                    }.bind(this);
                    xhr.open("POST","api/upload");
                    var data = new FormData();
                    data.append("srccontent", editForm.srccontent.files[0]);
                    xhr.send(data);
                }
                else {
                    // create and go back to previus view
                    if (!mediaItem.created) {
                        mediaItem.create(() => {
                            this.previousView();
                        });
                    }
                    else {
                        // update the mediaItem
                        mediaItem.update(() => {
                            this.viewProxy.update({item: mediaItem});
                            this.previousView();
                        });
                    }
                }
                return false;
            });

            // If url input completed, set attributes to image element and MediaItem
            this.viewProxy.bindAction("urlInputCompleted", () => {
                mediaItem.contentType = "image/png";
                mediaItem.srcType = "url";
                this.viewProxy.update({item: mediaItem});
                var img = this.root.getElementsByTagName("img")[0];
                img.src = mediaItem.src;
            });

            // set url on img or video element, depending on contentType
            this.viewProxy.bindAction("uploadInputCompleted", () => {
                console.log("upload finished");
                var img = this.root.getElementsByTagName("img")[0];
                var fileinput = this.root.querySelector("input[type='file']");
                mediaItem.contentType = fileinput.files[0].type;
                var mediaElement = null;
                this.viewProxy.update({item: mediaItem});
                if (mediaItem.mediaType == 'video') {
                    mediaElement = this.root.getElementsByTagName("video")[0];
                } else {
                    mediaElement = this.root.getElementsByTagName("img")[0];
                }
                var url = URL.createObjectURL(fileinput.files[0]);
                // set object url as source
                mediaElement.src = url;
            });

            // call the superclass once creation is done
            super.oncreate(callback);

        }
        //stop playing the mediaItem when changing the view
        onpause (callback) {
            var video = this.root.querySelector("video");
            if (video) {
                console.log("calling pause() on video: " + video);
                video.pause();
            }
            super.onpause(callback);
        }

        /*
         * for views with listviews: bind a list item to an item view
         * TODO: delete if no listview is used or if databinding uses ractive templates
         */
        bindListItemView(viewid, itemview, item) {
            // TODO: implement how attributes of item shall be displayed in itemview
        }

        /*
         * for views with listviews: react to the selection of a listitem
         * TODO: delete if no listview is used or if item selection is specified by targetview/targetaction
         */
        onListItemSelected(listitem, listview) {
            // TODO: implement how selection of listitem shall be handled
        }

        /*
         * for views with listviews: react to the selection of a listitem menu option
         * TODO: delete if no listview is used or if item selection is specified by targetview/targetaction
         */
        onListItemMenuItemSelected(option, listitem, listview) {
            // TODO: implement how selection of option for listitem shall be handled
        }

        /*
         * for views with dialogs
         * TODO: delete if no dialogs are used or if generic controller for dialogs is employed
         */
        bindDialog(dialogid, dialog, item) {
            // call the supertype function
            super.bindDialog(dialogid, dialog, item);

            // TODO: implement action bindings for dialog, accessing dialog.root
        }
    }
    // and return the view controller function
    return EditviewViewController;
});