/**
 * @author Jörn Kreutel
 * Edited by Nils Kretschmer / Laszlo Pal
 *
 */
define(["mwf", "entities", "mwfUtils"], function (mwf, entities, mwfUtils) {

    class ListviewViewController extends mwf.ViewController {

        constructor() {
            super();
            console.log("ListviewViewController()");

            this.addNewMediaItem = null;
            this.resetDatabaseElement = null;
            this.mediaItem = null;
            this.viewProxy = null;
        }

        /*
         * for any view: initialise the view
         */
        oncreate(callback) {

            //element for function change the view
            this.addNewMediaItemElement = this.root.querySelector("#addNewMediaItem");
            //click on selected element will open the next view
            this.addNewMediaItemElement.onclick = (() => {
                this.nextView("mediaEditview",{item: new entities.MediaItem()});
            });
            //initialising the element for databse
            this.resetDatabaseElement = this.root.querySelector("#resetDatabase");

            //function for reseting database
            this.resetDatabaseElement.onclick = (() => {
                if (confirm("Soll die Datenbank wirklich zurückgesetzt werden?")) {
                    indexedDB.deleteDatabase("mwftutdb");
                }
            });
            //element and function for CRUD operations
            this.addListener(new mwf.EventMatcher("crud","deleted","MediaItem"),((event) => {
                    this.removeFromListview(event.data);
                })
            );
            this.addListener(new mwf.EventMatcher("crud","created","MediaItem"),((event) => {
                this.addToListview(event.data);
            }));
            this.addListener(new mwf.EventMatcher("crud","updated","MediaItem"),((event) => {
                this.updateInListview(event.data._id,event.data);
            }));
            this.addListener(new mwf.EventMatcher("crud","deleted","MediaItem"),((event) => {
                this.removeFromListview(event.data);
            }));

            // toggle CRUD-Operations if Webserver available
            mwfUtils.isWebserverAvailable((callback) => {
                this.toggleServerElement = this.root.querySelector("#toggleServer");
                if (callback) {
                    this.toggleServerElement.onclick = (() => {
                        this.dbStatusElement = this.root.querySelector("#dbStatus");
                        var currentCRUD = this.application.currentCRUDScope;

                        if (currentCRUD == "local") {
                            this.application.switchCRUD("remote");
                            this.dbStatusElement.textContent = "remote";
                            entities.MediaItem.readAll((items) => {
                                this.initialiseListview(items);
                            });
                        } else {
                            this.application.switchCRUD("local");
                            this.dbStatusElement.textContent = "local";
                            entities.MediaItem.readAll((items) => {
                                this.initialiseListview(items);
                            });
                        }
                        console.log("Switched CRUD-Operation from " + currentCRUD + " to " + this.application.currentCRUDScope);
                    });
                } else {
                    this.toggleServerElement.onclick = (() => {
                        mwfUtils.showToast("Webserver unavailable", 3500);
                    });
                }
            });
            entities.MediaItem.readAll((items) => {
                this.initialiseListview(items);
            });

            // call the superclass once creation is done
            super.oncreate(callback);
        }
        //function for delete
        deleteItem(item) {
            item.delete(() => {
            });
        }
        //function for deleting/editing items in pop-up dialog
        editItem(item) {
            this.showDialog("mediaItemDialog", {
                item: item,
                actionBindings: {
                    submitForm: ((event) => {
                        event.original.preventDefault();
                        item.update(() => {
                        });
                        this.hideDialog();
                    }),
                    deleteItem: ((event) => {
                        this.deleteItem(item);
                        this.hideDialog();
                    })
                }
            });
        }
        //function for delete existed item in pop-up dialog
        confirmDeleteItem(item) {
            this.showDialog("deleteItemDialog", {
                item: item,
                actionBindings: {
                    cancelForm: ((event) => {
                        event.original.preventDefault();
                        this.hideDialog();
                    }),
                    deleteItem: ((event) => {
                        this.deleteItem(item);
                        this.hideDialog();
                    })
                }
            });
        }
        //next view via list elements
        onListItemSelected(listitem, listview) {
            // TODO: implement how selection of listitem shall be handled
            this.nextView("mediaReadview",{item: listitem});
        }
        /*
         * for views with listviews: react to the selection of a listitem menu option
         */
        onListItemMenuItemSelected(option, listitem, listview) {
            // TODO: implement how selection of option for listitem shall be handled
            super.onListItemMenuItemSelected(option, listitem,
                listview);
        }
        //on return from deleted elements no more existing in the actually view
        onReturnFromSubview(subviewid,returnValue,returnStatus,callback
        ) {
            if (subviewid == "mediaReadview" && returnValue &&
                returnValue.deletedItem) {
                this.removeFromListview(returnValue.deletedItem._id);
            }
            callback();
        }
        /*
         * for views with dialogs
         * TODO: delete if no dialogs are used or if generic controller for dialogs is employed
         */
        bindDialog(dialogid, dialog, item) {
            // call the supertype function
            super.bindDialog(dialogid, dialog, item);

            // TODO: implement action bindings for dialog, accessing dialog.root
        }
    }
    // and return the view controller function
    return ListviewViewController;
});